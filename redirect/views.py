from django.shortcuts import render
from django.http import JsonResponse
from django.core.cache import cache

from redirect.models import Redirect

# Create your views here.
def redirects(request):
    if request.method == 'GET':
        return redirects_get(request)


def redirects_get(request):
    id = request.GET.get('id', 0)
    cache_time = 86400
    redirect = cache.get(id)
    
    if redirect is None:
        redirect = Redirect.objects.filter(id=id).only('key', 'url').first()
    
    if redirect is not None:
        cache.set(id, redirect, cache_time)
        response = {'key': redirect.key, 'url': redirect.url}
    else:
        response = {'message': 'unavailable redirect'}
    
    return JsonResponse(response)
