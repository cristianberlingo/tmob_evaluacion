from django.db import models

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.cache import cache


class Redirect(models.Model):
    key = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    active = models.BooleanField()
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'redirect'


@receiver(post_save, sender=Redirect) 
def add_to_cache(sender, instance, created, raw, using, update_fields, **kwargs):
    if instance.active:
	    cache.set(instance.id, instance, 86400)
