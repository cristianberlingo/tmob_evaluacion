from django.db.models.signals import post_save
from django.dispatch import receiver

from django.contrib.auth.models import User

from redirect.models import Redirect

@receiver(post_save, sender=Redirect) 
def add_to_cache(sender, instance, **kwargs):
	cache.set(instance.id, instance, 86400)


@receiver(post_save, sender=User) 
def add_to_cache(sender, instance, **kwargs):
	print('instance: {}'.format(instance))


@receiver(post_save, sender=Redirect) 
def add_to_cache(sender, instance, **kwargs):
	cache.set(instance.id, instance, 86400)

# @receiver(post_save, sender=Redirect) 
# def add_to_cache(sender, instance, created, **kwargs):
# 	cache.set(instance.id, instance, 86400)
